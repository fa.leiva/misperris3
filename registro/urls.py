from django.urls import path
from . import views
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [    
    path('',views.index,name="index"),
    path('login/',views.login, name="login"),
    path('persona/registro',views.registro, name="registro"),
    path('login/userLogin', views.userLogin, name="userLogin"),
    path('login/userLogout', views.userLogout, name="userLogout"),
    path('perros/', views.perros, name="perros"),
    path('perros/registroPerro', views.registroPerro, name="registroPerro"),
    path('perros/modificarPerro', views.modificarPerro, name="modificarPerro"),
    path('perros/eliminarPerro/<int:id>', views.eliminarPerro, name="eliminarPerro"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)