from django.db.models import Q 
from rest_framework import generics, mixins
from registro.models import Persona, Perro
from .permissions import IsOwnerOrReadOnly
from .serializers import PersonaSerializer, PerroSerializer

#Persona

class PersonaAPIView(mixins.CreateModelMixin, generics.ListAPIView):

    lookup_field = 'id'
    serializer_class = PersonaSerializer
    #queryset = Person.objects.all()

    def get_queryset(self):
        qs = Persona.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                    Q(usuario__icontains=query)|
                    Q(email__icontains=query)
                    ).distinct()
        return qs

    def performCrear(self, serializer):
        serializer.save(user=self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class PersonaRUD(generics.RetrieveUpdateDestroyAPIView):

    lookup_field = 'id'
    serializer_class = PersonaSerializer
    permission_classes = [IsOwnerOrReadOnly]
    #queryset = Person.objects.all()

    def get_queryset(self):
        return Persona.objects.all()

#Perro

class PerroAPIView(mixins.CreateModelMixin, generics.ListAPIView):

    lookup_field = 'id'
    serializer_class = PerroSerializer

    def get_queryset(self):
        qs = Perro.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                    Q(nombre__icontains=query)|
                    Q(estado__icontains=query)
                    ).distinct()
        return qs

    def performCrear(self, serializer):
        serializer.save(user=self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class PerroRUD(generics.RetrieveUpdateDestroyAPIView):

    lookup_field = 'id'
    serializer_class = PerroSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def get_queryset(self):
        return Perro.objects.all()