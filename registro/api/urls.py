from django.conf.urls import url, include
from .views import PersonaRUD, PersonaAPIView, PerroRUD, PerroAPIView
app_name="registro"

urlpatterns = [
    url(r'^$', PersonaAPIView.as_view(),name='post-crear'),
    url(r'^(?P<id>\d+)/$', PersonaRUD.as_view(),name='post-rud'),
    url(r'^perros/$', PerroAPIView.as_view(),name='post-crearPerro'),
    url(r'^perros/(?P<id>\d+)/$', PerroRUD.as_view(),name='post-rudPerro'),
]
