from rest_framework import serializers
from registro.models import Persona, Perro

class PersonaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Persona
        fields = '__all__'

    def validarUsuario(self, value):
        qs = Persona.objects.filter(Usuario__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.id)
        if qs.exist():
            raise serializers.ValidationError("El usuario debe ser unico")
        return value

class PerroSerializer(serializers.ModelSerializer):

    class Meta:
        model = Perro
        fields = '__all__'