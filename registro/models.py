from django.db import models
from django.contrib.auth.models import User

class Persona(models.Model):
    usuario = models.CharField(max_length = 40, unique = True, default = "none")
    nombre = models.CharField(max_length=50)
    run = models.CharField(max_length = 15)    
    fechaNacimiento = models.CharField(max_length = 10)
    telefono = models.IntegerField()
    email = models.EmailField(max_length=30)
    contrasenia = models.CharField(max_length = 50, default = "contraseña")
    region = models.CharField(max_length = 60)
    comuna = models.CharField(max_length = 60)
    vivienda = models.CharField(max_length = 60)

    def __str__(self):
        return "persona"

class Perro(models.Model):
    nombre = models.CharField(max_length = 60)
    raza = models.CharField(max_length = 40)    
    descripcion = models.CharField(max_length = 200)
    estado = models.CharField(max_length = 50)
    foto = models.ImageField(upload_to = 'perros/fotos/')

    def __str__(self):
        return "perro"