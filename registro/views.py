from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona, Perro
from django.shortcuts import redirect
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
#API
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Create your views here.


def index(request):
    return render(request,'index.html',{'persona': Persona.objects.all(), 'perro': Perro.objects.all()})

def login(request):
    return render(request,'login.html',{'Registro de personas':Persona.objects.all()})

def registro(request):
    usuario = request.POST.get('usuario', '')    
    nombre = request.POST.get('nombre', '')
    run = request.POST.get('run', '')
    fechaNacimiento = request.POST.get('fechaNacimiento', '')
    telefono = request.POST.get('telefono', 0)
    email = request.POST.get('email', '')
    contrasenia = request.POST.get('contrasenia', '')
    region = request.POST.get('region', '')
    comuna = request.POST.get('comuna', '')
    vivienda = request.POST.get('vivienda', '')

    persona = Persona(usuario = usuario,nombre = nombre, run = run, fechaNacimiento = fechaNacimiento, telefono = telefono, email = email, contrasenia = contrasenia, region = region, comuna = comuna, vivienda = vivienda)
    persona.save()

    user = User.objects.create_user(usuario, email, contrasenia)
    user.save()

    return redirect('index')


def userLogin(request):
    username = request.POST.get('usuario', '')
    password = request.POST.get('contrasenia', '')
    user = authenticate(request, username=username, password=password)
   
    if user is not None:
        if user.is_superuser:
            auth_login(request, user)
            return redirect('registroPerro')
        else:
            auth_login(request, user)
            return redirect('index')
    else:
        return redirect('login')


def userLogout(request):
    logout(request)
    return redirect('index')


def perros(request):
    return render(request, 'perros.html', {'persona': Persona.objects.all(), 'perro': Perro.objects.all()})

def registroPerro(request):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    descripcion = request.POST.get('descripcion', '')
    foto = request.FILES.get('foto', False)
    estado = request.POST.get('estado', '')

    perro = Perro(nombre = nombre, raza = raza, foto = foto, descripcion = descripcion, estado = estado)
    perro.save()

    return redirect('perros')

def modificarPerro(request):
    id = request.POST.get('id', 0)
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')    
    descripcion = request.POST.get('descripcion', '')
    foto = request.FILES.get('foto', False)
    estado = request.POST.get('estado', '')

    perro = Perro.objects.get(pk = id)

    perro.nombre = nombre
    perro.raza = raza
    perro.foto = foto
    perro.descripcion = descripcion
    perro.estado = estado
    perro.save()

    return redirect('perros')

def eliminarPerro(request, id):
    perro = Perro.objects.get(pk = id)
    perro.delete()

    return redirect('perros')



